package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;
import org.junit.Test;
import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.exception.BufferIndexOutOfBoundsException;

public class BufferTest {

	@Test
	public void testEmptyBuffer() {
		Buffer buffer = new Buffer(10);
		assertTrue(buffer.isEmpty());
		assertEquals(10, buffer.capacity());
	}

	@Test
	public void testFullBuffer() {
		Buffer buffer = new Buffer(10);
		buffer.fill();
		assertTrue(buffer.isFull());
	}

	@Test(expected = BufferFullException.class)
	public void addElementToFullBuffer() throws BufferFullException {
		Buffer buffer = new Buffer(5);
		buffer.fill();
		buffer.addElement(new Element(10));
	}

	@Test
	public void testAddOneElementInBuffer() throws Exception {
		Buffer buffer = new Buffer(10);
		buffer.addElement(new Element(5));
		assertEquals(10, buffer.capacity());
		assertEquals(5, buffer.at(0).getData());
	}

	@Test
	public void testAddTwoElementsInBuffer() throws Exception {
		Buffer buffer = new Buffer(8);
		buffer.addElement(new Element(5));
		buffer.addElement(new Element(7));
		assertEquals(8, buffer.capacity());
		assertEquals(5, buffer.at(0).getData());
		assertEquals(7, buffer.at(1).getData());
	}

	@Test(expected = BufferEmptyException.class)
	public void removeOneElementFromEmptyList() throws BufferEmptyException {
		Buffer buffer = new Buffer(5);
		buffer.removeElement();
	}

	@Test
	public void removeOneElement() throws BufferEmptyException, BufferFullException {
		Buffer buffer = new Buffer(5);
		buffer.addElement(new Element(9));
		buffer.addElement(new Element(8));
		buffer.addElement(new Element(7));
		assertEquals(9, buffer.removeElement().getData());
		assertEquals(2, buffer.getCurrentLoad());
	}

	@Test(expected = BufferIndexOutOfBoundsException.class)
	public void getElementAtInvalidIndex() throws BufferIndexOutOfBoundsException {
		Buffer buffer = new Buffer(10);
		buffer.at(5);
	}
}
